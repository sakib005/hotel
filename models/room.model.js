const Room = function (room) {
  if (typeof room.id != "undefined") {
    this.id = room.id;
  }
  this.room_number = room.room_number;
  this.price = room.price;
  this.locaed = room.locaed;
  this.max_person = room.max_person;
  this.room_type = room.room_type;
};

module.exports = Room;
