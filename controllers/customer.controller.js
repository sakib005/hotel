const User = require("../models/user.model.js");
const bcrypt = require("bcryptjs");
const db = require("../models/db");

// Register a new User
exports.register = async (req, res) => {
    
    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hasPassword = await bcrypt.hash(req.body.password, salt);
    console.log(hasPassword);

    // Create an user object
    const user = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        mobile: req.body.mobile,
        password: hasPassword
    });
    // Save User in the database
    try {
        await db.registration(user, function(result){
            res.send(result.recordset[0]);
        });
    }
    catch (err){
        res.status(500).send(err);
    }    
};