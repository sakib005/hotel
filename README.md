# hotel
**install** express, nodemon, mssql, bcryptjs, jsonwebtoken

**change config file:**
module.exports = {
  server: "YOUR SERVER ADDRESS", -------> Give your server addess
  port: 1433,
  user: "YOUR USER NAME", --------> user name
  password: "YOUR SERVER PASSWORD", -------------- > user password
  database: "GPHakaton",
  connectionTimeout: 150000,
  driver: "tedious",
  options: {
    appName: "GPHakaton",
    encrypt: false,
    enableArithAbort: false,
  },
  pool: {
    max: 20,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  TOKEN_SECRET: "XGtjIOp_RRvNA",
};

**Create Table:**
</br>
**#Booking Table**

USE [GPHakaton]
CREATE TABLE [dbo].[tbl_Booking](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[room_id] [bigint] NULL,
	[customer_id] [bigint] NULL,
	[arrival_time] [datetime] NULL,
	[checkout_time] [datetime] NULL,
	[book_type] [nvarchar](100) NULL,
	[book_time] [datetime] NULL,
 CONSTRAINT [PK_tbl_Booking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

**#Customer Table**
CREATE TABLE [dbo].[tbl_Customer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](100) NULL,
	[last_name] [nvarchar](100) NULL,
	[email] [nvarchar](100) NULL,
	[phone] [nvarchar](100) NULL,
	[password] [nvarchar](max) NULL,
	[registered_time] [datetime] NULL,
 CONSTRAINT [PK_tbl_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

**#Payment Table**

CREATE TABLE [dbo].[tbl_Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[booking_id] [bigint] NULL,
	[customer_id] [bigint] NULL,
	[amount] [decimal](18, 0) NULL,
	[date] [datetime] NULL,
 CONSTRAINT [PK_tbl_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

**#Room Table**

CREATE TABLE [dbo].[tbl_Room](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[room_number] [nvarchar](50) NULL,
	[price] [decimal](18, 0) NULL,
	[locked] [nvarchar](50) NULL,
	[max_person] [int] NULL,
	[room_type] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_Room] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



#### After creating table run the project.... 

#NOTE: API Documentation added to project in api documentation folder.



