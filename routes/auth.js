const router = require('express').Router();
const userController = require('../controllers/customer.controller');
const bookingController = require('../controllers/booking.controller');
const paymentController = require('../controllers/payment.controller');

// Register a new User
router.post('/register', userController.register);

//Booking
router.post('/booking', bookingController.booking);
router.get('/booking-list', bookingController.booking_list);

//Payment
router.post('/payment', paymentController.payment);
router.post('/due-payment', paymentController.due_payment);

//Checkout
router.post('/checkout', bookingController.checkout);

module.exports = router;