const { json } = require("express");
const sql = require("mssql");
const util = require("util");
const config = require("../config/config.js");

// Create a connection to the database
var dbConfig = {
  server: config.server,
  port: config.port,
  user: config.user,
  password: config.password,
  database: config.database,
  connectionTimeout: config.connectionTimeout,
  driver: config.driver,
  options: {
    appName: config.options.appName,
    encrypt: config.options.encrypt,
    enableArithAbort: config.options.enableArithAbort,
  },
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    idleTimeoutMillis: config.pool.idleTimeoutMillis,
  },
};

const registration = async function (req, res) {
 
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }
    const request = new sql.Request();
    request.input("first_name", sql.NVarChar(100), req.first_name);
    request.input("last_name", sql.NVarChar(100), req.last_name);
    request.input("email", sql.NVarChar(100), req.email);
    request.input("phone", sql.NVarChar(100), req.mobile);
    request.input("password", sql.NVarChar(100), req.password);
    request.input("registered_time", sql.DateTime, new Date());
    request.query(
      "insert into [GPHakaton].[dbo].[tbl_Customer] (first_name,last_name,email,phone,password,registered_time) values (@first_name,@last_name,@email,@phone,@password,@registered_time); SELECT SCOPE_IDENTITY() AS id;",
      (err, result) => {
        console.dir(result.recordset[0].id);
        res(result);
      }
    );
  });
};

const booking = async function(req, res){
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }

    console.log(req);

    const request = new sql.Request();
    request.input("room_id", sql.BigInt, req.room_id);
    request.input("customer_id", sql.BigInt, req.customer_id);
    request.input("arrival_time", sql.DateTime, req.arrival_time);
    request.input("checkout_time", sql.DateTime, req.checkout_time);
    request.input("book_type", sql.NVarChar(100), req.book_type);
    request.input("book_time", sql.DateTime, new Date());
    request.query(
      "insert into [GPHakaton].[dbo].[tbl_Booking] (room_id,customer_id,arrival_time,checkout_time,book_type,book_time) values (@room_id,@customer_id,@arrival_time,@checkout_time,@book_type,@book_time); SELECT SCOPE_IDENTITY() AS id;",
      (err, result) => {
        console.dir(result.recordset[0].id);
        res(result.recordset[0]);
      }
    );
  });
}

const check_room_available_or_not = async (req, res)=>{
  await sql.connect(dbConfig, (err)=> {
    if(err){
      return res(false);
    }
    console.log(req);
    const request = new sql.Request();
    request.input('room_id', sql.BigInt, req.room_id);
    request.input('arrival_time', sql.DateTime, new Date(req.arrival_time));
    request.input('checkout_time', sql.DateTime, new Date(req.checkout_time));
    request.query("select * from [GPHakaton].[dbo].[tbl_Booking] where room_id = @room_id and (arrival_time between @arrival_time and @checkout_time or checkout_time between @arrival_time and @checkout_time) and book_type = 'ongoing'", 
    (err, result) => {
      console.log(result)
      res(result);
    });

  })
}

const booking_list = async function(res){
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }
    const request = new sql.Request();
    request.query(
      "select CONCAT(c.first_name, ' ', c.last_name) CustomerName, r.room_number RoomNumber, b.arrival_time ArrivalTime, b.checkout_time CheckoutTime, p.amount Amount, (r.price -p.amount) Due from tbl_Booking b left join tbl_Customer c on b.customer_id = c.Id left join tbl_Room r on r.Id = b.room_id left join tbl_Payment p on b.Id = p.booking_id where b.book_type = 'ongoing'",
      (err, result) => {
        console.dir(result.recordsets);
        res(result.recordsets);
      }
    );
  });
}

const payment = async function(req, res){
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }

    const request = new sql.Request();
    request.input("booking_id", sql.BigInt, req.booking_id);
    request.input("customer_id", sql.BigInt, req.customer_id);
    request.input("amount", sql.Decimal, req.amount);
    request.input("payment_date", sql.DateTime, new Date());
    request.query(
      "insert into [GPHakaton].[dbo].[tbl_Payment] (booking_id,customer_id,amount,date) values (@booking_id,@customer_id,@amount,@payment_date); SELECT SCOPE_IDENTITY() AS id;",
      (err, result) => {
        console.log(err);
        console.log(result);
        res(result);
      }
    );
  });
}

const due_payment = async function(req, res){
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }

    const request = new sql.Request();
    request.input("booking_id", sql.BigInt, req.booking_id);
    request.input("customer_id", sql.BigInt, req.customer_id);
    request.input("amount", sql.Decimal, req.amount);
    request.input("payment_date", sql.DateTime, new Date());
    request.query(
      "update [GPHakaton].[dbo].[tbl_Payment] set amount = amount+ @amount where booking_id = @booking_id and customer_id = @customer_id",
      (err, result) => {
        console.dir(result);
        res("success");
      }
    );
  });
}

const check_payment_status= async function(room_id, res){
  await sql.connect(dbConfig, (err) => {
    if (err) {
      return res(false);
    }

    const request = new sql.Request();
    request.input("room_id", sql.BigInt, room_id);
    request.query(
      "select (rr.price-pp.amount) due from tbl_Booking bo join tbl_Room rr on bo.room_id = rr.Id left join tbl_Payment pp on bo.Id = pp.booking_id where bo.room_id = @room_id and bo.book_type = 'ongoing'",
      (err, result) => {
        res(result.recordset);
      }
    );
  });
}

const checkout = async function(room_id, res){

await sql.connect(dbConfig, (err) => {
  if (err) {
    return res(false);
  }

  const request = new sql.Request();
  request.input("room_id", sql.BigInt, room_id);
  request.query(
    "update tbl_Booking set book_type = 'finished' where room_id = @room_id and book_type = 'ongoing'",
    (err, result) => {
      console.log(result.rowsAffected[0]);
      res(result.rowsAffected);
    }
  );
});
}


module.exports = {
  registration,
  booking,
  booking_list,
  check_room_available_or_not,
  payment,
  due_payment,
  check_payment_status,
  checkout
};
