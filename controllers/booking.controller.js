const Booking = require("../models/booking.model.js");
const db = require("../models/db");

exports.booking = async (req, res) => {
  try {
    const booking = new Booking({
      room_id: req.body.roomid,
      customer_id: req.body.customerid,
      arrival_time: req.body.arrival_time,
      checkout_time: req.body.checkout_time,
      book_type: req.body.book_type,
    });
    console.log(booking);
    await db.check_room_available_or_not(booking, async function (result) {
        console.log(result.recordsets[0].length);
      if (result.recordsets[0].length > 0) {
        res.send("Sorry Room Not Available");
      } else {
        await db.booking(booking, (rr)=> {
            res.send(rr);
        })
      }
    });
  } catch (err) {
    res.status(500).send(err);
  }
};

exports.booking_list = async (req, res) => {
  try {
    await db.booking_list(function (result) {
      var bookingList = { BookingList: result[0] };
      res.send(bookingList);
    });
  } catch (err) {
    res.status(500).send(err);
  }
};

exports.checkout = async (req, res) => {
try {
  const booking = new Booking({
    room_id: req.body.roomid,
  });
  await db.check_payment_status(booking.room_id, async function (result) {
    if (typeof result[0] != "undefined") {
      if(result[0].due == 0){
        await db.checkout(booking.room_id, function(resu){
          if(resu[0] > 0){
            res.send("Successfully Checkout");
          }else{
            res.send("Already Checkedout");
          }
        })
      }else{
        res.send("You have due amount of = " + result[0].due + " TK Please Pay it.");
      }
      
    } else {
      res.send("Sorry Room not Found");
    }
  });
} catch (err) {
  res.status(500).send(err);
}
};