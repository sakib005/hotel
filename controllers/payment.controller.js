const Payment = require("../models/payment.model.js");
const db = require("../models/db");

exports.payment = async (req, res) => {
    
    const payment = new Payment({
        booking_id: req.body.booking_id,
        customer_id: req.body.customer_id,
        amount: req.body.amount,
    });

    console.log(payment);
    // Save User in the database
    try {
        await db.payment(payment, function(result){
            res.send(result.recordset[0]);
        });
    }
    catch (err){
        res.status(500).send(err);
    }    
};

exports.due_payment = async (req, res) => {
    
    const payment = new Payment({
        booking_id: req.body.booking_id,
        customer_id: req.body.customer_id,
        amount: req.body.amount,
    });
    // Save User in the database
    try {
        await db.due_payment(payment, function(result){
            res.send(result);
        });
    }
    catch (err){
        res.status(500).send(err);
    }    
};

