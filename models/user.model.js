
const User = function(user) {
    if( typeof user.id != 'undefined' ) {
        this.id = user.id;
    }
    this.first_name = user.first_name;
    this.last_name = user.last_name;
    this.email = user.email;
    this.mobile = user.mobile;
    this.password = user.password;
    if( typeof user.registered_time != 'undefined' ) {
        this.registered_time = user.register_time;
    }
};


module.exports = User;