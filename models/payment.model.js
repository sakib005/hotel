const Payment = function (payemnt) {
  if (typeof payemnt.id != "undefined") {
    this.id = payemnt.id;
  }
  this.booking_id = payemnt.booking_id;
  this.customer_id = payemnt.customer_id;
  this.amount = payemnt.amount;
  if (typeof payemnt.payment_date != "undefined") {
    this.payment_date = payemnt.payment_date;
  }
};

module.exports = Payment;
