module.exports = {
  server: "YOUR SERVER ADDRESS",
  port: 1433,
  user: "YOUR USER NAME",
  password: "YOUR SERVER PASSWORD",
  database: "GPHakaton",
  connectionTimeout: 150000,
  driver: "tedious",
  options: {
    appName: "GPHakaton",
    encrypt: false,
    enableArithAbort: false,
  },
  pool: {
    max: 20,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  TOKEN_SECRET: "XGtjIOp_RRvNA",
};
