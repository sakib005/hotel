const Booking = function (booking) {
  if (typeof booking.id != "undefined") {
    this.id = booking.id;
  }
  this.room_id = booking.room_id;
  this.customer_id = booking.customer_id;
  this.arrival_time = booking.arrival_time;
  this.checkout_time = booking.checkout_time;
  this.book_type = booking.book_type;
  if (typeof booking.book_time != "undefined") {
    this.book_time = booking.book_time;
  }
};

module.exports = Booking;